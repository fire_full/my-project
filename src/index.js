import React from "react";
import ReactDOM from "react-dom";
import App from "./App";
import "./assets/fonts/BRoya.ttf";
import "./assets/fonts/Roboto-Thin.ttf";
import "./assets/css/main.css";
import { getDirection } from "./localization";
import * as serviceWorker from "./serviceWorker";

document.getElementsByTagName("body")[0].setAttribute("dir", getDirection());
// style.direction = getDirection();
console.log("---> ", document.getElementsByTagName("body")[0]);

ReactDOM.render(<App />, document.getElementById("root"));
serviceWorker.register();
