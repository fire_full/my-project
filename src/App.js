import React from 'react';
import { BrowserRouter as Router } from "react-router-dom";
import {ThemeProvider , StylesProvider} from '@material-ui/core/styles'
import { CssBaseline, jssPreset } from '@material-ui/core';
import IndexScreen from './screens/index';
import {create} from 'jss';
import rtl from 'jss-rtl';
import {getDirection} from './localization/index'
import theme from './theme/theme';
import './App.css'

const jss = create ({ plugins : [...jssPreset().plugins , rtl()] })


function App(){
  console.log(getDirection());
  
 return getDirection() === "ltr"  ? (
  <Router>
    <ThemeProvider theme={theme}>
      <StylesProvider>
        <CssBaseline/>
        <IndexScreen/>
      </StylesProvider>
    </ThemeProvider>
    </Router>
 ) : (
  <Router>
  <ThemeProvider theme={theme}>
  <StylesProvider jss={jss} >
    <CssBaseline/>
    <IndexScreen/>
  </StylesProvider>
</ThemeProvider>
</Router>
 )

}

export default App
