import en from './en'
import fa from './fa'


const lang = localStorage.getItem("lang") ?  localStorage.getItem("lang") : 'en';
export{ lang }
const direction ={
    fa : "rtl" ,
    en : "ltr"
}

function getDirection(){
    return direction[lang];
}
export { getDirection }

const fonts ={
    fa : 'IranSans',
    en : 'Roboto'
}
function getFonts(){
    return fonts[lang] 
}
export { getFonts }

const translates = {
    en : en ,
    fa : fa
}

function getTranslate(){
    return translates[lang]
}
export { getTranslate }

function changeLanguage(newLang){
   if( newLang === lang){
       return
   }
   localStorage.setItem('lang' , newLang);
   window.location.reload();
}
export { changeLanguage }