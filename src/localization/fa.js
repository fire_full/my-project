import Image1 from "../assets/images/wp4923981.jpg";

export default {
  name: "امیر علی حاتمی ",
  home: "خانه",
  about: "درباره ما",
  resume: "رزومه",
  portfolios: "گالری",
  contact: "تماس با ما",
  hi: "سلام من ",
  hi2: "هستم",
  desc:
    "برنامه نویس فرانت هستم مسلط به HTML , CSS, Sass , JS , Jquery, React و علاقه مند به یادگیری و تمرکز بیشتر برای کار جهت توسعه نرم افزار  ",
  aboutMe: "معرفی",
  services: "خدمات",
  fullName: "نام",
  age: "سن",
  Nationality: "ملیت",
  Languages: "زبان",
  phoneNumber: "شماره تلفن",
  Iran: "ایران",
  Persian: "فارسی",
  numberP: "989190707875+",
  download: "  دانلود  فایل",
  webDesign: "طراحی وب سایت",
  webDevelop: "توسعه وب",
  MobileAppp: "اپلیکیشن موبایل",
  desc1:
    " امیرعلی حاتمی در زمینه طراحی سایت و بهره گیری از بروزترین تکنولوژی های طراحی وب با دقت و تخصص در کنار شماست و تا دستیابی به بهترین نتایج شما را همراهی خواهد کرد،",
  mySkills: "مهارت ها",
  TitleResume1: "HTML 5",
  TitleResume2: "CSS3",
  TitleResume3: "JS",
  TitleResume4: "React",
  TitleResume5: "Jquery",
  TitleResume6: "PHP",
  myResume: "رزومه",
  contactMe: "تماس با ما",
  titlePhone: "تلفن",
  Info2Phone: "989381208287+",
  Info1Phone: "989190707875+",
  titleEmail: "ایمیل",
  Info2Email: "amiralihatami220@gmail.com",
  Info1Email: "amiralihatami21@gmail.com",
  titleAddress: "آدرس",
  Info1Address: "121 King Street, Melbourne, Victoria 3000, Australia",
  portfoliosList: [
    {
      id: 1,
      title: "ری اکت",
      image: Image1,
      desc: "طراحی سایت ری اکت",
      href: "https://www.google.com",
    },
    {
      id: 2,
      title: "طراحی سایت",
      image: Image1,
      desc: "طراحی سایت html , css",
      href: "https://www.google.com",
    },
    {
      id: 3,
      title: "جی کوئری",
      image: Image1,
      desc: "آموزش جی کوئری ",
      href: "https://www.google.com",
    },
    {
      id: 4,
      title: "جاوا اسکریپت",
      image: Image1,
      desc: "آموزش جاوا اسکریپت",
      href: "https://www.google.com",
    },
  ],
  portfolio: "گالری",
  sendData: "ارسال",
};
