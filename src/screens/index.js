import React, { useState } from "react";
import CssBaseline from "@material-ui/core/CssBaseline";
import Drawer from "@material-ui/core/Drawer";
import Hidden from "@material-ui/core/Hidden";
import IconButton from "@material-ui/core/IconButton";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import MenuIcon from "@material-ui/icons/Menu";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import { getTranslate, changeLanguage, lang } from "../localization/index";
import PImage from "../assets/images/amirali.jpg";
//import Amir from '../assets/amir.jpeg'
import { Route, Switch, NavLink } from "react-router-dom";
import HomeScreens from "./homeScreens";
import AboutScreens from "./aboutScreens";
import ResumeScreens from "./resumeScreens";
import PrtfolisScreens from "./portfolisScreens";
import ContactScreens from "./contactScreens";
import "./index.css";
import "./home.css";

const drawerWidth = 260;

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    color: "#fff",
  },

  drawer: {
    [theme.breakpoints.up("md")]: {
      width: drawerWidth,
      flexShrink: 0,
    },
  },
  menuButton: {
    zIndex: "999",
    margin: theme.spacing(2),
    marginLeft: 0,
    backgroundColor: theme.palette.primary.backgroundColor,
    cursor: "pointer",
    width: 50,
    height: 50,
    [theme.breakpoints.up("md")]: {
      display: "none",
    },

    position: "fixed",
    top: 20,
    border: "1px solid #fff",
    borderRadius: "2px",
  },
  // necessary for content to be below app bar
  topDrawer: {
    width: "100%",
    padding: "20px ",
    borderBottom: "1px solid #2e344e",
    textAlign: "center",
  },
  profileImage: {
    width: "200px",
    height: "200px",
    maxWidth: "100%",
    borderRadius: "100px",
    border: "3px solid #fefefe",
    verticalAlign: "middle",
  },
  drawerPaper: {
    width: drawerWidth,
    backgroundColor: "#191d2b",
    height: "100vh",
    borderRight: "1px solid #2e344e",
  },
  content: {
    flexGrow: 1,
    paddingLeft: theme.spacing(3),
    paddingRight: theme.spacing(3),
    position: "relative",
    textAlign: "center",
  },
  colorText: {
    color: "#c4c4c4",
  },
  bottomDrawer: {
    borderTop: "1px solid #2e344e",
    padding: "15px",
    textAlign: "center",
  },
  colorI: {
    color: theme.palette.primary.backgroundColor,
    backgroundColor: "green",
  },

  activeMenuItem: {
    borderRight: "5px solid #037fff",
    backgroundColor: `${theme.palette.primary.main} !important`,
    color: "#fff !important",
    "& .overlay": {
      transition: "unset",
    },
  },
}));

interface Props {
  /**
   * Injected by the documentation to work in an iframe.
   * You won't need it on your project.
   */
  window?: () => Window;
}

export default function ResponsiveDrawer(props: Props) {
  const { window } = props;
  const classes = useStyles();
  //const [page , setPage] = useState(0);
  const [mobileOpen, setMobileOpen] = useState(false);
  const translate = getTranslate();

  const handleDrawerToggle = () => {
    setMobileOpen(!mobileOpen);
  };

  const drawer = (
    <>
      <div className={classes.topDrawer}>
        <img
          src={PImage}
          className={classes.profileImage}
          alt={translate.name}
        />
      </div>

      <List
        style={{
          display: "flex",
          flexDirection: "column",
          flex: 1,
          justifyContent: "center",
          alignItems: "center",
          width: "100%",
        }}
      >
        {/* inb sharte zir baraye hover roy element haye kenary safhe hastesh */}
        <ListItem
          className={"listitem"}
          onClick={() => {
            setMobileOpen(false);
          }}
          style={{ paddingLeft: 0, paddingRight: 0 }}
          button
          component={NavLink}
          to="/"
          exact
          activeClassName={classes.activeMenuItem}
        >
          <ListItemText
            disableTypography={true}
            children={
              <Typography
                variant="body2"
                className="listitemText"
                style={{ textAlign: "center", color: "#fff" }}
              >
                {translate.home}
              </Typography>
            }
          />
          <div className="overlay" />
        </ListItem>

        <ListItem
          className={"listitem"}
          onClick={() => {
            setMobileOpen(false);
          }}
          style={{ paddingLeft: 0, paddingRight: 0 }}
          button
          component={NavLink}
          to="/about"
          activeClassName={classes.activeMenuItem}
        >
          <ListItemText
            disableTypography={true}
            children={
              <Typography
                variant="body2"
                className="listitemText"
                style={{ textAlign: "center", color: "#fff" }}
              >
                {translate.about}
              </Typography>
            }
          />
          <div className="overlay" />
        </ListItem>

        <ListItem
          className={"listitem"}
          onClick={() => {
            setMobileOpen(false);
          }}
          style={{ paddingLeft: 0, paddingRight: 0 }}
          button
          component={NavLink}
          to="/resume"
          activeClassName={classes.activeMenuItem}
        >
          <ListItemText
            disableTypography={true}
            children={
              <Typography
                variant="body2"
                className="listitemText"
                style={{ textAlign: "center", color: "#fff" }}
              >
                {translate.resume}
              </Typography>
            }
          />
          <div className="overlay" />
        </ListItem>

        <ListItem
          className={"listitem"}
          onClick={() => {
            setMobileOpen(false);
          }}
          style={{ paddingLeft: 0, paddingRight: 0 }}
          button
          component={NavLink}
          to="/portfolios"
          activeClassName={classes.activeMenuItem}
        >
          <ListItemText
            disableTypography={true}
            children={
              <Typography
                variant="body2"
                className="listitemText"
                style={{ textAlign: "center", color: "#fff" }}
              >
                {" "}
                {translate.portfolios}
              </Typography>
            }
          />
          <div className="overlay" />
        </ListItem>

        <ListItem
          className={"listitem"}
          onClick={() => {
            setMobileOpen(false);
          }}
          style={{ paddingLeft: 0, paddingRight: 0 }}
          button
          component={NavLink}
          to="/contact"
          activeClassName={classes.activeMenuItem}
        >
          <ListItemText
            disableTypography={true}
            children={
              <Typography
                variant="body2"
                className="listitemText"
                style={{ textAlign: "center", color: "#fff" }}
              >
                {translate.contact}
              </Typography>
            }
          />
          <div className="overlay" />
        </ListItem>
      </List>

      <div className={classes.bottomDrawer} style={{ color: "#fff" }}>
        <Button
          size="small"
          variant="outlined"
          color={lang === "en" ? "primary" : "secondary"}
          onClick={() => changeLanguage("en")}
          style={{ color: "#fff" }}
        >
          EN
        </Button>
        {" / "}
        <Button
          size="small"
          variant="outlined"
          color={lang === "fa" ? "primary" : "secondary"}
          onClick={() => changeLanguage("fa")}
          style={{ color: "#fff" }}
        >
          FA
        </Button>
      </div>
    </>
  );

  const container =
    window !== undefined ? () => window().document.body : undefined;

  return (
    <div className={classes.root}>
      <CssBaseline />

      <IconButton
        color="inherit"
        aria-label="open drawer"
        edge="start"
        onClick={handleDrawerToggle}
        className={classes.menuButton}
      >
        <MenuIcon />
      </IconButton>

      <nav className={classes.drawer} aria-label="mailbox folders">
        {/* The implementation can be swapped with js to avoid SEO duplication of links. */}
        <Hidden mdUp>
          <Drawer
            container={container}
            variant="temporary"
            open={mobileOpen}
            onClose={handleDrawerToggle}
            classes={{
              paper: classes.drawerPaper,
            }}
            ModalProps={{
              keepMounted: true, // Better open performance on mobile.
            }}
          >
            {drawer}
          </Drawer>
        </Hidden>
        <Hidden smDown>
          <Drawer
            classes={{
              paper: classes.drawerPaper,
            }}
            variant="permanent"
            open
          >
            {drawer}
          </Drawer>
        </Hidden>
      </nav>
      <main className={classes.content}>
        <span className="line1"></span>
        <span className="line2"></span>
        <span className="line3"></span>
        <span className="line4"></span>

        <Switch>
          <Route path="/" exact>
            <HomeScreens />
          </Route>
          <Route path="/about">
            <AboutScreens />
          </Route>
          <Route path="/resume">
            <ResumeScreens />
          </Route>

          <Route path="/portfolios">
            <PrtfolisScreens />
          </Route>

          <Route path="/contact">
            <ContactScreens />
          </Route>
        </Switch>
      </main>
    </div>
  );
}
