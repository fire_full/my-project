import React from 'react'
import { makeStyles } from '@material-ui/core/styles';
import {Grid, Typography} from '@material-ui/core'
import Skill from '../components/Skill'
import Title from '../components/Title'
import {getTranslate} from '../localization/index'
import BusinessCenterIcon from '@material-ui/icons/BusinessCenter';
import MyStepper from '../components/Stepper'
import LocalLibraryIcon from '@material-ui/icons/LocalLibrary';



const useStyles= makeStyles((theme)=>({

     root:{
        paddingTop:'60px',
        paddingRight:'25px',
        paddingLeft:'25px',
     },
     textRoot:{
         padding:theme.spacing(4),
     },
     resume:{
         marginTop:'50px',
     },
})
)


function ResumeScreens(){
    const classes = useStyles();
    const translate = getTranslate();
   return(
<Grid container item xs={12} className={classes.root}>

  <Grid container direction='row'>
      <Title title={translate.mySkills}/>
   <Grid item xs={12} md={6} lg={6} className={classes.textRoot}>
      <Skill value={100} Title={translate.TitleResume1}/>
      <Skill value={100} Title={translate.TitleResume2}/>
      <Skill value={95}  Title={translate.TitleResume3}/>
   </Grid>
   <Grid item xs={12} md={6} lg={6} className={classes.textRoot}>
      <Skill value={90} Title={translate.TitleResume4}/>
      <Skill value={80} Title={translate.TitleResume5}/>
      <Skill value={10} Title={translate.TitleResume6}/>
   </Grid>
   </Grid>

   <Grid container item xs={12}>
       
       <div  className={classes.resume}>
       <Title title={translate.myResume}/>

       </div>

   <Grid container direction='row' alignItems="center" justify="flex-start">
     <BusinessCenterIcon color="secondary"/>
     <Typography variant="h2" style={{margin:'0 10px'}}>
         Working Experience
     </Typography>
     <Grid container direction="row">
         <MyStepper />
     </Grid>
   </Grid>


   <Grid container direction='row' alignItems="center" justify="flex-start" style={{marginTop:'10px'}}>
     <LocalLibraryIcon color="secondary"/>
     <Typography variant="h2" style={{margin:'0 10px'}}>
         Educational Experience
     </Typography>
     <Grid container direction="row">
         <MyStepper container />
     </Grid>
   </Grid>

  </Grid>


</Grid>

)
}

export default ResumeScreens