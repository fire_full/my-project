import React, { useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import { Grid, Typography, TextField, Button } from "@material-ui/core";
import { getTranslate } from "../localization/index";
import Title from "../components/Title";
import "./contact.css";
import Adresscontact from "../components/Adresscontact";
import PhoneIcon from "@material-ui/icons/Phone";
import EmailIcon from "@material-ui/icons/Email";
import LocationOnIcon from "@material-ui/icons/LocationOn";

const useStyles = makeStyles((theme) => ({
  root: {
    //minHeight:'100vh',
  },
  contactGrid: {
    paddingTop: "60px",
    paddingRight: "25px",
    paddingLeft: "25px",
    position: "relative",
    [theme.breakpoints.down("xs")]: {
      paddingRight: "15px",
      paddingLeft: "15px",
    },
  },
  contactForm: {},
  getInTouch: {
    textAlign: "left",
  },
  ButtonContact: {
    textAlign: "left",
    marginBottom: "20px",
  },
}));

function ContactScreens() {
  const classes = useStyles();
  const translate = getTranslate();

  return (
    // <Grid container className={classes.root}>
    //   <Grid item  xs={12} lg={12} className={classes.contactGrid}>
    //     <Title title={translate.contactMe} />
    //     <Grid container direction="row">
    //       <Grid item xs={10} md={6} className={classes.contactForm}>
    //         <Typography variant="h2" className={classes.getInTouch}>
    //           {" "}
    //           Get In Touch
    //         </Typography>
    //         <TextField
    //           style={{ marginTop: "25px" }}
    //           size="medium"
    //           fullWidth
    //           required
    //           label="Enter Your Name"
    //           variant="outlined"
    //         />
    //         <TextField
    //           style={{ marginTop: "25px" }}
    //           size="medium"
    //           fullWidth
    //           required
    //           label="Enter Your Email"
    //           variant="outlined"
    //         />
    //         <TextField
    //           style={{ marginTop: "25px" }}
    //           size="medium"
    //           fullWidth
    //           required
    //           label="Enter Your Subject"
    //           variant="outlined"
    //         />
    //         <TextField
    //           style={{ marginTop: "25px" }}
    //           size="medium"
    //           fullWidth
    //           required
    //           label="Enter Your Message"
    //           variant="outlined"
    //           multiline={true}
    //           rows={8}
    //         />
    //         <div className={classes.ButtonContact}>
    //           <Button
    //             size="large"
    //             variant="contained"
    //             color="secondary"
    //             className="buttonSendData"
    //           >
    //             {translate.sendData}
    //             <div className="dealay"></div>
    //           </Button>
    //         </div>
    //       </Grid>
    //       <Grid
    //         item
    //         xs={10}
    //         md={6}
    //         className={classes.detailsGrid}
    //         style={{ marginTop: "10px" }}
    //       >
    //         <Adresscontact
    //           titleContact={translate.titlePhone}
    //           Info2={translate.Info2Phone}
    //           Info1={translate.Info1Phone}
    //           icon={<PhoneIcon />}
    //         />
    //         <Adresscontact
    //           titleContact={translate.titleEmail}
    //           Info2={translate.Info2Email}
    //           Info1={translate.Info1Email}
    //           icon={<EmailIcon />}
    //         />
    //         <Adresscontact
    //           titleContact={translate.titleAddress}
    //           Info2={translate.Info2Address}
    //           Info1={translate.Info1Address}
    //           icon={<LocationOnIcon />}
    //         />
    //       </Grid>
    //     </Grid>
    //   </Grid>
    // </Grid>
    <div>contact</div>
  );
}

export default ContactScreens;
