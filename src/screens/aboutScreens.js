import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import { Grid, Typography, Button } from "@material-ui/core";
import { getTranslate, getDirection } from "../localization/index";
import Title from "../components/Title";
import MainImage from "../assets/images/wp4923981.jpg";
import Services from "../components/Services";
import PhoneIphoneIcon from "@material-ui/icons/PhoneIphone";
import CodeIcon from "@material-ui/icons/Code";
import LocalFloristIcon from "@material-ui/icons/LocalFlorist";

const useStyles = makeStyles((theme) => ({
  root: {
    //minHeight:'100vh',
  },
  aboutGrid: {
    paddingTop: "60px",
    paddingRight: "25px",
    paddingLeft: "25px",
    position: "relative",
  },
  mainImage: {
    width: "100%",
    height: "100%",
  },
  mainAbout: {
    position: "relative",
    "&::after": {
      content: "''",
      position: "absolute",
      left: 0,
      height: "50%",
      width: 10,
      backgroundColor: "#037fff",
    },
    "&::before": {
      content: "''",
      position: "absolute",
      right: 0,
      bottom: 0,
      height: "50%",
      width: 10,
      backgroundColor: "rgba(3, 127, 255, 0.6)",
    },
  },
  nameText: {
    color: theme.palette.primary.main,
  },
  GridText: {
    textAlign: getDirection() === "rtl" ? "justify" : "left",
    paddingLeft: "20px",
    paddingRight: "20px",
    [theme.breakpoints.down("xs")]: {
      paddingLeft: 0,
      paddingRight: 0,
    },
  },
  iconMain: {
    color: theme.palette.primary.main,
    fontSize: "40px",
  },
}));

function AboutScreens() {
  const classes = useStyles();
  const translate = getTranslate();
  return (
    <Grid
      container
      className={`${classes.root} ${classes.aboutGrid}`}
      direction="row"
    >
      <Title title={translate.aboutMe} />
      <Grid container>
        <Grid item xs={12} md={6} className={classes.mainAbout}>
          <img
            src={MainImage}
            alt={translate.name}
            className={classes.mainImage}
          />
        </Grid>
        <Grid
          item
          xs={12}
          md={6}
          className={classes.GridText}
          style={{ marginTop: "10px" }}
        >
          <Typography variant="h3" style={{ marginTop: "5px" }}>
            {translate.hi}{" "}
            <span className={classes.nameText}>{translate.name}</span>
            {translate.hi2}
          </Typography>

          <Typography
            variant="body1"
            className="TextIconStroke"
            style={{ marginTop: "10px" }}
          >
            {translate.desc}
          </Typography>
          <Typography
            variant="body1"
            className="TextIconStroke"
            style={{ marginTop: "10px" }}
          >
            <b style={{ width: "120px", display: "inline-block" }}>
              {translate.fullName}{" "}
            </b>

            {translate.name}
          </Typography>
          <Typography
            variant="body1"
            className="TextIconStroke"
            style={{ marginTop: "10px" }}
          >
            <b style={{ width: "120px", display: "inline-block" }}>
              {translate.age}{" "}
            </b>
            24
          </Typography>
          <Typography
            variant="body1"
            className="TextIconStroke"
            style={{ marginTop: "10px" }}
          >
            <b style={{ width: "120px", display: "inline-block" }}>
              {translate.Nationality}{" "}
            </b>

            {translate.Iran}
          </Typography>
          <Typography
            variant="body1"
            className="TextIconStroke"
            style={{ marginTop: "10px" }}
          >
            <b style={{ width: "120px", display: "inline-block" }}>
              {translate.Languages}{" "}
            </b>

            {translate.Persian}
          </Typography>
          <Typography
            variant="body1"
            className="TextIconStroke"
            style={{ marginTop: "10px" }}
          >
            <b style={{ width: "120px", display: "inline-block" }}>
              {translate.phoneNumber}{" "}
            </b>

            {translate.numberP}
          </Typography>
          <div style={{ display: "flex", marginTop: "20px" }}>
            <Button
              variant="contained"
              color="primary"
              style={{ color: "#000" }}
            >
              {translate.download}
            </Button>
          </div>
        </Grid>
      </Grid>

      <Grid item container xs={12} lg={12} className={classes.aboutGrid}>
        <Title title={translate.services} />
      </Grid>
      <Grid container direction="row">
        <Services
          icon={<LocalFloristIcon className={classes.iconMain} />}
          title={translate.webDesign}
          desc={translate.desc1}
        />

        <Services
          icon={<CodeIcon className={classes.iconMain} />}
          title={translate.webDevelop}
          desc={translate.desc1}
        />

        <Services
          icon={<PhoneIphoneIcon className={classes.iconMain} />}
          title={translate.MobileAppp}
          desc={translate.desc1}
        />
      </Grid>
    </Grid>
  );
}

export default AboutScreens;
