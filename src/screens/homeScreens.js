import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import IconButton from "@material-ui/core/IconButton";
import { Grid, Typography, Button } from "@material-ui/core";
import "./home.css";
import TelegramIcon from "@material-ui/icons/Telegram";
import LinkedInIcon from "@material-ui/icons/LinkedIn";
import InstagramIcon from "@material-ui/icons/Instagram";
import { getTranslate, lang } from "../localization/index";
const useStyles = makeStyles((theme) => ({
  root: {
    height: "98vh",
    overflowY: "hidden !important",
    width: "100%",
    display: "flex !important",
    //  justifyContent: "center",
    //  alignItems: "center",
    flexDirection: lang === "fa" ? "row-reverse" : "row",
  },
  nameText: {
    color: theme.palette.primary.main,
  },
  typeText: {
    display: "block",
  },
  divIcon: {
    marginTop: "15px",
  },
  textTypoGhraphy: {
    [theme.breakpoints.down("lg")]: {
      marginTop: "50px",
      display: "block",
    },
  },
  spanIcon: {
    border: "1px solid #2e344e",
    margin: "0 10px 0 10px",
    color: "#808080",
    width: "50px",
    height: "50px",
    borderRadius: "50%",
    lineHeight: "45px",
    textAlign: "center",
    transition: ".5s",
    "&:hover": {
      borderColor: theme.palette.primary.main,
      transition: ".5s",
      color: theme.palette.primary.main,
      boxShadow: "0 0 5px #31ea9f",
      transform: "sclae(1.1)",
      textShadow: "0 0 5px #31ea9f",
    },
  },
}));

function HomeScreens() {
  const classes = useStyles();
  const translate = getTranslate();
  return (
    <div className={classes.root}>
      <div id="stars"></div>
      <div id="stars2"></div>
      <div id="stars3"></div>

      <Grid
        container
        item
        justify="center"
        direction="column"
        alignItems="center"
        xs={12}
        className={`${classes.root} ${classes.textTypoGhraphy}`}
      >
        <div style={{ display: "flex", flexDirection: "column" }}>
          <Typography variant="h1">
            {translate.hi}{" "}
            <span className={classes.nameText}>{translate.name}</span>
            {translate.hi2}
          </Typography>

          <Typography variant="h2" className="TextIconStroke">
            {translate.desc}
          </Typography>

          <div className={classes.divIcon}>
            <IconButton
              href="https://t.me/fire_full"
              className={classes.spanIcon}
            >
              <TelegramIcon />
            </IconButton>
            <IconButton
              href="https://www.linkedin.com/in/amir-ali-hatami-a183791b7/"
              className={classes.spanIcon}
            >
              <LinkedInIcon />
            </IconButton>
            <IconButton
              href="https://www.instagram.com/amiralihatami21/"
              className={classes.spanIcon}
            >
              <InstagramIcon />
            </IconButton>
          </div>
        </div>
      </Grid>
    </div>
  );
}

export default HomeScreens;
