import React, { useState, useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";
import { Grid, TextField, Button } from "@material-ui/core";
import { getTranslate } from "../localization/index";
import Title from "../components/Title";
import Portfolio from "../components/Portfolio";
// import ContentShop from "../components/ContentShop";
// import axios from "axios";

const useStyles = makeStyles((theme) => ({
  root: {
    padding: "60px 30px 30px 30px",
    width: "100%",
    [theme.breakpoints.down("xs")]: {
      paddingRight: "10px",
      paddingLeft: "10px",
    },
  },
}));

function PortfolisScreens() {
  const classes = useStyles();
  const translate = getTranslate();
  // const [data, setData] = useState([]);
  // const [errors, setErrors] = useState("");
  // const [price, setPrice] = useState("");
  // const [image, setImage] = useState("");
  // const [title, setTitle] = useState("");

  // useEffect(() => {
  //   fetch("http://localhost:3001/")
  //     .then((response) => {
  //       return response.json();
  //     })
  //     .then((data) => setData(data));
  // }, []);

  // const onDeleteClick = (id) => {
  //   axios.delete("http://localhost:3001/delete?id=" + id).then((response) => {
  //     if (response.status === 200) {
  //       setData(response.data.data);
  //     }
  //   });
  // };

  // let status;
  // const addData = () => {
  //   fetch("http://localhost:3001/add", {
  //     method: "POST",
  //     headers: {
  //       "Content-Type": "application/json",
  //     },
  //     body: JSON.stringify({
  //       title: title,
  //       price: price,
  //       image: image,
  //     }),
  //   })
  //     .then((response) => {
  //       status = response.status;
  //       return response.json();
  //     })
  //     .then((responseJson) => {
  //       if (status === 201) {
  //         setData(responseJson.data);
  //       } else if (status === 400) {
  //         console.log(responseJson.errors);
  //       } else {
  //         console.log(responseJson);
  //       }
  //     });
  // };

  return (
    <div className={classes.root}>
      <Title title={translate.portfolio} />
      <Grid
        container
        item
        xs={12}
        direction="row"
        justify="flex-start"
        alignItems="center"
      >
        {translate.portfoliosList.map((portfolo) => (
          <Portfolio
            key={portfolo.id}
            title={portfolo.title}
            desc={portfolo.desc}
            image={portfolo.image}
            href={portfolo.href}
          />
        ))}
      </Grid>

      {/* <Grid
        container
        item
        xs={12}
        direction="row"
        justify="flex-start"
        alignItems="center"
        style={{ marginTop: "10px", color: "#fff" }}
      >
        {data.map((item) => {
          return (
            <ContentShop
              key={item.id}
              title={item.title}
              image={item.image}
              price={item.price}
              ondelete={() => onDeleteClick(item.id)}
            />
          );
        })}
      </Grid> */}

      {/* <Grid container item>
        <Grid lg={3} md={3} sm={6}>
          <TextField
            style={{ margin: "10px", color: "#fff" }}
            value={title}
            onChange={(e) => setTitle(e.target.value)}
            lable="عنوان"
          />
          <TextField
            style={{ margin: "10px" }}
            value={price}
            onChange={(e) => setPrice(e.target.value)}
            lable="قیمت"
          />
          <TextField
            value={image}
            style={{ margin: "10px", color: "#fff" }}
            onChange={(e) => setImage(e.target.value)}
            lable="عکس"
          />
          <Button onClick={addData}> Add </Button>
        </Grid>
      </Grid> */}
      {/* <div>
        {errors.map((e) => (
          <li key={e.key}>
            {e.key} : {e.errorText}
          </li>
        ))}
      </div> */}
    </div>
  );
}

export default PortfolisScreens;
