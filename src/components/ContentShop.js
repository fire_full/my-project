import React from "react";
import { Grid, Button, Typography } from "@material-ui/core";
import { makeStyles } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  root: {
    padding: 5,
    color: theme.palette.primary.main,
    display: "flex",
    flexDirection: "column",
    textAlign: "left",
  },
  imageShop: {
    width: "100%",
    height: "100%",
  },
}));

function ContentShop({ image, title, price, ondelete }) {
  const classes = useStyles();
  return (
    <>
      <Grid lg={4} md={6} sm={6} xs={12} className={classes.root}>
        <img src={image} className={classes.imageShop} alt="port folio" />
        <Typography variant="h4" style={{ lineHeight: "1" }}>
          {title}
        </Typography>
        <Typography variant="h4" style={{ lineHeight: "1" }}>
          {price} تومان
        </Typography>
        <Button onClick={ondelete}>حذف</Button>
      </Grid>
    </>
  );
}
export default ContentShop;
