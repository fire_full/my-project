import React from 'react'
import { makeStyles } from '@material-ui/core/styles'
import {Typography} from '@material-ui/core'
// import {getTranslate} from '../localization/index'
 

const useStyles = makeStyles((theme)=>({
    title :{
       textAlign: 'left',
       '&::after':{
        content: "''",
        position: "absolute",
        left:0,
        top:"auto",
        bottom: 0,
        height:5,
        borderRadius: 100,
        width: 30,
        backgroundColor: "#037fff",
       },
       '&::before':{
        content: "''",
        position: "absolute",
        left:0,
        top:"auto",
        bottom: 0,
        height:5,
        borderRadius: 100,
        width: 100,
        backgroundColor: "rgba(3, 127, 255, 0.1)",
       }

    },
    subTitle:{
     fontSize:'2rem',
     position:"absolute",
     left: 15,
     top:"90%",
     lineHeight:"1rem",
     fontWeight: "900",
     color:'rgba(25,29,43,.44)',
    // display:'inline-block',
     zIndex:-1,
    }
})
)

function Title({title}){
    const classes = useStyles();
    return(
        <div style={{position:'relative',marginBottom:'60px', height:60, width:"100%"}}>
          <Typography variant="h2" className={classes.title}> {title}</Typography>
          <span className={classes.subTitle}>{title}</span>
        </div>
    )
}
export default Title