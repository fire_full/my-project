import React,{useState , useEffect} from 'react'
import { makeStyles, createStyles } from '@material-ui/core/styles';
import {Typography} from '@material-ui/core'
import {getTranslate} from '../localization/index'
import LinearProgress  from '@material-ui/core/LinearProgress';



const useStyles= makeStyles((theme) => ({
    
    root:{
        width:"100%" ,
        padding:10 ,
        display:'flex',
        flexDirection:'column',
        alignItems:'flex-start',
        

    },
    progress:{
        display:'flex',
        justifyContent:'flex-start',
        alignItems:'baseline',
        width:"100%",
    },
    mainProgress:{
        width:"70%",
        marginLeft:"15px",
        backgroundColor:'#2e344e',
        height:6
    },
    
    

})
)

function Skill( {value , Title} ){
    // const value = 50;
    const classes = useStyles()
    const translate = getTranslate()
    const [ val,setVal ] = useState(sessionStorage.val ? value : 0)
    //sessionStorage.getItem('val') ? parseInt(sessionStorage.getItem('val')) : 0
    useEffect(()=>{
       
        // if(val <= value){
            var timer = (setTimeout(()=>{

                if(val === value){
                 sessionStorage.setItem("val" , value);     
                 return
                }
            setVal(val => val + 5)
          },100))
        // } 
    },[val])

    
    

    return(
       <div className={classes.root}>
         <Typography variant='h3'>
             {Title} 
         </Typography>

         <div className={classes.progress}>
               <Typography variant="body1" >
                 {value}%
               </Typography>
               <LinearProgress className={classes.mainProgress} value={val} variant="determinate" color="primary" ></LinearProgress>
         </div>

       </div>
   )
}
export default Skill