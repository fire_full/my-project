import React from 'react'
import {makeStyles} from '@material-ui/core/styles';
import {Grid, Typography } from '@material-ui/core'




const useStyles = makeStyles((theme)=>({
      root:{
          paddingRight:"25px",
          paddingLeft:'25px',
        //   marginTop:'30px',
          marginBottom:"10px"
      },
      main:{
          backgroundColor:"#191d2b",
        //   height:"250px",
          zIndex:1,
          textAlign:'left',
          border:'1px solid #2e344e',
          borderTop:'5px solid #2e344e',
          padding:theme.spacing(8),
          transition:'.5s',
          '&:hover':{
              borderTopColor: theme.palette.primary.main,
              transition:'.5s',
          }
      },
     
      title:{
       marginTop:"20px",
       color:'#fff',
       position:'relative',
       paddingBottom:'15px',
       '&::before':{
           content:'""',
           position:'absolute',
           left:0,
           top:"auto",
           bottom:0,
           height:"2px",
           width:'50px',
           backgroundColor:'#2e344e'
       }
      },
      desc:{
          marginTop:'10px' ,
          
      }
    
})
)

function Services({title,desc,icon}){
    const classes = useStyles();
  return(
      <Grid item lg={4} md={6} sm={6} xs={12} className={classes.root}>
<Grid className={classes.main} direction='column' >
  {icon}
  <Typography variant="h2" className={classes.title}> {title}</Typography>
 <Typography varaint="h4" className={classes.desc}>
 {desc}
 </Typography>
</Grid>
      </Grid>
  )
}
export default Services