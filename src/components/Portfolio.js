import React, { useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import { Grid, Typography, Modal } from "@material-ui/core";
import Port from "../assets/images/wp4923981.jpg";

const useStyles = makeStyles((theme) => ({
  root: {
    padding: 5,
    color: theme.palette.primary.main,
    display: "flex",
    flexDirection: "column",
  },
  imgSet: {
    width: "100%",
    height: "100%",
    cursor: "pointer",
    position: "relative",
  },
  textSetInfo: {
    lineHeight: 1,
    marginTop: 5,
    transition: ".5s",
    textAlign: "left",
    "& a": {
      textDecoration: "none",
      color: "#fff",
      fontWeight: "bolder",
      transition: ".5s",
      "&:hover": {
        color: theme.palette.primary.main,
        transition: ".5s",
      },
    },
  },
  textSetInfo1: {
    lineHeight: 1,
    marginTop: 5,
    transition: ".5s",
    textAlign: "left",
    fontWeight: 600,
    color: "#fff",
  },
  ModalImage: {
    width: "80%",
    height: "80%",
    position: "absolute",
    margin: "auto",
    top: "10%",
    left: "10%",
    padding: "10px",
    [theme.breakpoints.down("xs")]: {
      padding: "5px",
      width: "80%",
      height: "40%",
      top: "35%",
      left: "10%",
    },
  },
}));

function Portfolio({ title, desc, image, href }) {
  const classes = useStyles();
  const [open, setOpen] = useState(false);
  return (
    <>
      <Grid lg={4} md={6} sm={6} xs={12} className={classes.root}>
        <img
          className={classes.imgSet}
          src={image}
          alt={title}
          onClick={() => setOpen(true)}
        />
        <Typography variant="h4" className={classes.textSetInfo}>
          {" "}
          <a href={href}>{title}</a>
        </Typography>
        <Typography variant="h6" className={classes.textSetInfo1}>
          {" "}
          {desc}{" "}
        </Typography>
      </Grid>
      <Modal open={open} onClose={() => setOpen(false)}>
        <img className={classes.ModalImage} src={image} alt={title} />
      </Modal>
    </>
  );
}
export default Portfolio;
