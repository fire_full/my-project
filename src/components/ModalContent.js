import { Button } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import React from "react";

const useStyles = makeStyles((theme) => ({
  rootModal: {
    //  display:'none',
    top: 0,
    left: 0,
    width: "100vw",
    height: "100vh",
    position: "fixed",
    backgroundColor: "rgba(0,0,0,0.9)",
    zIndex: 10000,
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  ContentModal: {
    // position: "fixed",
    // top: 0,
    // right: 0,
    // left: 0,
    // bottom: 0,
    // margin: "auto",
    height: "400px",
    width: "50%",
    backgroundColor: "#fff",
    zIndex: 11000,
    color: "#000",
  },
}));

export default function ModalContent({ onClose }) {
  const classes = useStyles();
  return (
    <div className={`${classes.rootModal}`}>
      <div
        style={{
          display: "flex",
          flexDirection: "column",
          justifyContent: "space-between ",
        }}
        className={classes.ContentModal}
      >
        <div>Hello</div>
        <Button
          variant="contained"
          color="primary"
          style={{
            textAlign: "center",
            margin: "10px auto",
            bottom: "0",
            width: "30%",
          }}
          onClick={onClose}
        >
          close
        </Button>
      </div>
    </div>
  );
}
