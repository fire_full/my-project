import React from "react";
import { Typography } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  contactInfo: {
    margin: "30px 15px 30px 15px",
    // width: "90%",
    padding: "25px",
    backgroundColor: "#191d2b",
    borderTop: "3px solid #555",
    display: "flex",
    alignItems: "center",
    transition: ".5s",
    "&:hover": {
      borderColor: theme.palette.primary.main,
      transition: ".5s",
    },
  },
  iconInfo: {
    padding: "18px",
    border: "1px solid #2e344e",
    color: "#aaa",
    fontSize: "20px",
    width: "60px",
    height: "60px",
  },
  titleInfo: {
    fontSize: "20px",
    padding: "0 10px",
    marginTop: "-10px",
    lineHeight: 2,
  },
  textInfo: {
    fontSize: "17px",
    padding: "0 20px",
    lineHeight: 1,
    color: "#fff",
    fontWeight: "bold",
    transition: ".5s",
    "&:hover": {
      transition: ".5s",
      cursor: "pointer",
      color: theme.palette.secondary.main,
    },
  },
}));

function Adresscontact({ titleContact, Info1, Info2, icon }) {
  const classes = useStyles();
  return (
    <div className={classes.contactInfo}>
      <span className={classes.iconInfo}>{icon}</span>
      <div style={{ margin: "0", padding: "0", boxShadow: "border-box" }}>
        <Typography className={classes.titleInfo} display="block" align="left">
          {titleContact}
        </Typography>
        <Typography className={classes.textInfo} display="block" align="left">
          {Info1}
        </Typography>
        <Typography className={classes.textInfo} display="block" align="left">
          {Info2}
        </Typography>
      </div>
    </div>
  );
}

export default Adresscontact;
