import React from "react";
import { makeStyles, Theme, createStyles } from "@material-ui/core/styles";
import Stepper from "@material-ui/core/Stepper";
import Step from "@material-ui/core/Step";
import StepLabel from "@material-ui/core/StepLabel";
import StepContent from "@material-ui/core/StepContent";
import StepConnector from "@material-ui/core/StepConnector";
import Typography from "@material-ui/core/Typography";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      width: "100%",
    },
    button: {
      marginTop: theme.spacing(1),
      marginRight: theme.spacing(1),
    },
    actionsContainer: {
      marginBottom: theme.spacing(2),
    },
    resetContainer: {
      padding: theme.spacing(3),
    },
    stepConnector: {
      paddingBottom: 0,
      "& span": {
        borderLeft: "3px solid  #2e344e",
      },
    },
    stepContent: {
      display: "flex",
      flexDirection: "row",
      borderLeft: "3px solid  #2e344e",
      marginTop: 0,
    },
    circle: {
      width: 16,
      height: 16,
      borderRadius: "8px",
      border: "5px solid  #2e344e",
      marginLeft: "5px",
    },
    stepLable: {
      display: "flex",
      flexDirection: "row",
    },
    lable: {
      display: "flex",
      flexDirection: "row",
    },
  })
);

// function getSteps() {
//   return ['Select campaign settings', 'Create an ad group', 'Create an ad'];
// }

export default function MyStepper() {
  const classes = useStyles();
  const steps = [
    { id: -1, date: "", Title: "", subtitle: "", content: " " },
    {
      id: 0,
      date: "2019-9-21",
      Title: "Master",
      subtitle: "university",
      content:
        "lorem ipsis color black darkest Try a test build and deploy, directly from your Git repository or a folder.",
    },
    {
      id: 1,
      date: "2019-9-21",
      Title: "Master",
      subtitle: "university",
      content:
        "lorem ipsis color black darkest Try a test build and deploy, directly from your Git repository or a folder.",
    },
    { id: -2, date: "", Title: "", subtitle: "", content: " " },
  ];

  return (
    <div className={classes.root}>
      <Stepper
        connector={<StepConnector className={classes.stepConnector} />}
        style={{ backgroundColor: "transparent" }}
        orientation="vertical"
      >
        {steps.map((step) =>
          step.id >= 0 ? (
            <Step active={true} key={step.id}>
              <StepLabel
                classes={{ lable: classes.lable }}
                className={classes.stepLable}
                icon={<span className={classes.circle} />}
              >
                <Typography>{step.date}</Typography>
                <span></span>
                <Typography>{step.Title}</Typography>
              </StepLabel>
              <StepContent className={classes.stepContent}>
                <Typography>{step.content}</Typography>
              </StepContent>
            </Step>
          ) : step.id === -1 ? (
            <Step active={true} key={step.id}>
              <StepLabel icon={null}>{step.Title}</StepLabel>
            </Step>
          ) : null
        )}
      </Stepper>
    </div>
  );
}
