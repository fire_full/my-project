import {createMuiTheme} from '@material-ui/core/styles'
import palete from './palete'
import {getDirection , getFonts} from "../localization/index"


const theme = createMuiTheme({
   palette : palete ,
   direction : getDirection() , 
   spacing : 2.5 ,
   typography :{
       h1 : {
         fontWeight : 400,
         fontSize   : "30px",
         lineHeight : "4rem",
         fontFamily : getFonts(),
       },
       h2 : {
        fontWeight : 400,
        fontSize   : "28px",
        // lineHeight : "4rem",
        fontFamily : getFonts(),
      },
      h3 : {
        fontWeight : 400,
        fontSize   : "18px",
        lineHeight : "1rem",
        fontFamily : getFonts(),
      },
      h4 : {
        fontWeight : 400,
        fontSize   : "16px",
        lineHeight : "4rem",
        fontFamily : getFonts(),
      },
      h5 : {
        fontWeight : 400,
        fontSize   : "14px",
        lineHeight : "4rem",
        fontFamily : getFonts(),
      },
      h6 : {
        fontWeight : 400,
        fontSize   : "12px",
        lineHeight : "4rem",
        fontFamily : getFonts(),
      },
      subtitle1 : {
         fontWeight : 400 , 
         fontSize : "12px" ,
         lineHeight : "3rem" ,
         fontFamily : getFonts(), 
         marginTop:'10px'
      },
      subtitle2 :{
        fontWeight : 400 , 
        fontSize : "10px" ,
        lineHeight : "3rem" ,
        fontFamily : getFonts(),
        marginTop:'10px'
      } ,
      body1 : {
        fontWeight : 400 , 
        fontSize : getDirection() ==='rtl' ? '18px' : '16px' ,
        lineHeight : "1.8rem" ,
        fontFamily : getFonts(),
        marginTop:'10px',
        marginBottom:"10px",
      },
      body2 : {
        fontWeight : 600 , 
        fontSize : ".94rem" ,
        color:'#000',
        fontFamily : getFonts(),
        marginTop:'10px',
        letterSpacing: 1,
        
      },
      color1:{
        color:'#a4acc4',
      }
   }

})
export default theme