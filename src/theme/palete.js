export default {
    primary: {
        main : "#31ea9f",
        contrastText : "#fff",
        backgroundColor :'#112257'
    },
    secondary : {
        main : "#f23571"
    },
    text : {
        primary : "#999999" ,
        secondary: "#c4c4c4"
    }
}   